package com.example.posts.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.example.posts.data.PostRepository
import com.example.posts.data.Posts

class SavedPostsListViewModel internal constructor(
    postRepository: PostRepository
) : ViewModel() {
    val posts: LiveData<List<Posts>> = postRepository.getAllPosts()
}