package com.example.posts.viewmodels

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.posts.data.CommentRepository
import com.example.posts.data.JsonPlaceholderRepository
import com.example.posts.data.PostRepository
import com.example.posts.data.UserRepository

class PostDetailViewModelFactory(
    private val jsonPlaceholderRepository: JsonPlaceholderRepository,
    private val postRepository: PostRepository,
    private val commentRepository: CommentRepository,
    private val userRepository: UserRepository,
    private val postId: Int
): ViewModelProvider.Factory {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return PostDetailViewModel(
            jsonPlaceholderRepository,
            postRepository,
            commentRepository,
            userRepository,
            postId
        ) as T
    }
}