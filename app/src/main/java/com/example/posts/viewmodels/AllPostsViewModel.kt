package com.example.posts.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.posts.data.JsonPlaceholderPost
import com.example.posts.data.JsonPlaceholderRepository
import com.example.posts.data.PostRepository
import com.example.posts.data.Posts
import com.example.posts.mappers.PostMapper
import kotlinx.coroutines.launch
import org.mapstruct.factory.Mappers.getMapper

class AllPostsListViewModel internal constructor(
    private val jsonRepository: JsonPlaceholderRepository,
    private val postRepository: PostRepository
) : ViewModel() {

    private val _posts = MutableLiveData<List<Posts>>()
    val posts: LiveData<List<Posts>> = _posts

    init {
        viewModelScope.launch {
            try {
                val result = jsonRepository.getPosts()
                val converter = getMapper(PostMapper::class.java)
                _posts.value = result
                    .map { converter.postGetResponseToPost(it) }
            } catch(t: Throwable) {
                _posts.value = postRepository.getAllPostsAsync()
            }
        }

    }

}