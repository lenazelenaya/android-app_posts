package com.example.posts.viewmodels

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.posts.data.*
import com.example.posts.mappers.CommentMapper
import com.example.posts.mappers.PostMapper
import com.example.posts.mappers.UserMapper
import kotlinx.coroutines.async
import kotlinx.coroutines.launch
import org.mapstruct.factory.Mappers.getMapper

private const val POST_DETAIL: String = "POST_DETAILS"

class PostDetailViewModel(
    private val jsonPlaceholderRepository: JsonPlaceholderRepository,
    private val postRepository: PostRepository,
    private val commentRepository: CommentRepository,
    private val userRepository: UserRepository,
    private val postId: Int
) : ViewModel() {

    private val _post = MutableLiveData<Posts>()
    private val _comments = MutableLiveData<List<Comment>>()
    private val _user = MutableLiveData<User>()

    val post: LiveData<Posts> = _post
    val comments: LiveData<List<Comment>> = _comments
    val user: LiveData<User> = _user

    init {
        viewModelScope.launch {
            try {
                _post.value = postRepository.getPostById(postId)
                _comments.value = commentRepository.getComment(postId)
                _user.value = userRepository.getUser(_post.value!!.userId)
            } catch (t: Throwable) {
                fetchPost()
            }
        }
    }

    fun fetchPost() {
        fetchPostDetails()
        fetchComments()
    }


    fun fetchPostDetails() {
        val postMapper = getMapper(PostMapper::class.java)
        val userMapper = getMapper(UserMapper::class.java)

        val postAsync = viewModelScope.async {
            jsonPlaceholderRepository.getPostById(postId)
        }

        viewModelScope.launch {
            try {
                val receivedPost = postAsync.await()
                _post.value = postMapper.postGetResponseToPost(receivedPost)

                val result = jsonPlaceholderRepository
                    .getUserById(receivedPost.userId)
                _user.value = userMapper.userGetResponseToUser(result)

            } catch (t: Throwable) {
                Log.w(POST_DETAIL, "Could not fetch post details")
            }
        }
    }

    fun savePostDetails() {
        viewModelScope.launch {
            val postToSave = post.value
            postToSave?.let {
                postRepository.savePost(postToSave)
            }
        }
        viewModelScope.launch {
            val commentsToSave = comments.value
            commentsToSave?.let {
                commentRepository.saveComments(commentsToSave)
            }
        }
        viewModelScope.launch {
            val userToSave = user.value
            userToSave?.let {
                userRepository.saveUser(userToSave)
            }
        }
    }

    fun fetchComments() {
        val commentMapper = getMapper(CommentMapper::class.java)
        viewModelScope.launch {
            try {
                _comments.value = jsonPlaceholderRepository
                    .getCommentsByPostId(postId)
                    .map { commentMapper.commentGetResponseToComment(it) }
            } catch (t: Throwable) {
                Log.w(POST_DETAIL, "Could not fetch comments")
            }
        }
    }
}