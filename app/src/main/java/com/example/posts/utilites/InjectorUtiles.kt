package com.example.posts.utilites

import android.content.Context
import com.example.posts.api.JsonPlaceholderService
import com.example.posts.data.*
import com.example.posts.viewmodels.AllPostsListViewModelFactory
import com.example.posts.viewmodels.PostDetailViewModelFactory
import com.example.posts.viewmodels.SavedPostsListViewModelFactory

object InjectorUtils {

    private fun getPostRepository(context: Context): PostRepository {
        return PostRepository.getInstance(
            AppDatabase.getInstance(context.applicationContext).postDao())
    }

    private fun getCommentRepository(context: Context): CommentRepository {
        return CommentRepository.getInstance(
            AppDatabase.getInstance(context.applicationContext).commentDao())
    }

    private fun getUserRepository(context: Context): UserRepository {
        return UserRepository.getInstance(
            AppDatabase.getInstance(context.applicationContext).userDao())
    }


    fun provideSavedPostsListViewModelFactory(
        context: Context
    ): SavedPostsViewModelFactory {
        return SavedPostsViewModelFactory(getPostRepository(context))
    }

    fun provideAllPostsListViewModelFactory(
        context: Context
    ): AllPostsListViewModelFactory {
        val repository = JsonPlaceholderRepository(JsonPlaceholderService.create())
        return AllPostsViewModelFactory(repository, getPostRepository(context))
    }

    fun providePostDetailViewModelFactory(
        context: Context,
        postId: String
    ): PostDetailViewModelFactory {
        val jsonPlaceholderRepository = JsonPlaceholderRepository(JsonPlaceholderService.create())
        return PostDetailViewModelFactory(
            jsonPlaceholderRepository,
            getPostRepository(context),
            getCommentRepository(context),
            getUserRepository(context),
            postId
        )
    }
}