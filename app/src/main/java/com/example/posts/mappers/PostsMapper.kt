package com.example.posts.mappers

import com.example.posts.data.JsonPlaceholderPost
import com.example.posts.data.Posts
import org.mapstruct.Mapper
import org.mapstruct.Mapping

@Mapper
interface PostMapper {
    @Mapping(target = "postId", source = "id")
    fun postGetResponseToPost(postGetResponse: JsonPlaceholderPost) : Posts
}