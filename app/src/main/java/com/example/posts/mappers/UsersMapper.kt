package com.example.posts.mappers

import com.example.posts.data.JsonPlaceholderUsers
import com.example.posts.data.User
import org.mapstruct.Mapper
import org.mapstruct.Mapping

@Mapper
interface UserMapper {
    @Mapping(target = "userId", source = "id")
    fun userGetResponseToUser(userGetResponse: JsonPlaceholderUsers) : User
}