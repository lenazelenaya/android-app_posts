package com.example.posts.mappers

import com.example.posts.data.Comment
import com.example.posts.data.JsonPlaceholderComment
import org.mapstruct.Mapper
import org.mapstruct.Mapping

@Mapper
interface CommentMapper {

    @Mapping(target = "commentId", source = "id")
    fun commentGetResponseToComment(commentGetResponse: JsonPlaceholderComment) : Comment
}