package com.example.posts.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.posts.data.JsonPlaceholderRepository
import com.example.posts.data.PostRepository

class AllPostsVMFactory(
    private val repository: JsonPlaceholderRepository,
    private val postRepository: PostRepository
) : ViewModelProvider.Factory {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return AllPostsVM(repository, postRepository) as T
    }
}