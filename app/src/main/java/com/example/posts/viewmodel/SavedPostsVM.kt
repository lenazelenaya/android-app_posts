package com.example.posts.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.example.posts.data.PostRepository
import com.example.posts.data.Posts

class SavedPostsVM internal constructor(
    postRepository: PostRepository
) : ViewModel() {
    val posts: LiveData<List<Posts>> = postRepository.getAllPosts()
}