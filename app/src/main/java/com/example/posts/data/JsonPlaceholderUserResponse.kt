package com.example.posts.data

import com.google.gson.annotations.SerializedName

data class JsonPlaceholderUserResponse (
    @field:SerializedName("results") val results: List<User>,
    @field:SerializedName("total_pages") val totalPages: Int
)