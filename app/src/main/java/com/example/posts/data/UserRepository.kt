package com.example.posts.data

class UserRepository private constructor(private val usersDao: UsersDao){
    fun getUsers() = usersDao.getUsers()

    fun getUser(userId: Int) = usersDao.getUserById(userId)

    suspend fun saveUser(user: User) = usersDao.insert(user)

    companion object {

        // For Singleton instantiation
        @Volatile private var instance: UserRepository? = null

        fun getInstance(usersDao: UsersDao) =
            instance ?: synchronized(this) {
                instance ?: UserRepository(usersDao).also { instance = it }
            }
    }
}