package com.example.posts.data

import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

interface CommentDao {
    @Query("SELECT * FROM comments WHERE postId = :postId")
    suspend fun getComment(postId: Int): List<Comment>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insertComment(comments: List<Comment>): Void

}