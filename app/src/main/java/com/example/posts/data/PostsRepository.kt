package com.example.posts.data


class PostRepository private constructor(
    private val postsDao: PostsDao
) {
    fun getAllPosts() = postsDao.getPosts()

    suspend fun getAllPostsAsync() = postsDao.getAllPostsAsync()

    suspend fun getPostById(postId: Int) = postsDao.getPost(postId)

    suspend fun savePost(post: Posts) = postsDao.insertAll(post)

    companion object {

        // For Singleton instantiation
        @Volatile private var instance: PostRepository? = null

        fun getInstance(postDao: PostsDao) =
            instance ?: synchronized(this) {
                instance ?: PostRepository(postDao).also { instance = it }
            }
    }
}