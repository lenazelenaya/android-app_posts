package com.example.posts.data

import com.example.posts.api.JsonPlaceholderService

class JsonPlaceholderRepository(private val service: JsonPlaceholderService) {

    suspend fun getPosts(): List<JsonPlaceholderPost> {
        return service.getPosts()
    }

    suspend fun getPostById(postId: Int) : JsonPlaceholderPost {
        return service.getPostById(postId)
    }

    suspend fun getCommentsByPostId(postId: Int): List<JsonPlaceholderComment> {
        return service.getCommentsByPostId(postId)
    }

    suspend fun getUserById(userId: Int): JsonPlaceholderUsers {
        return service.getUserById(userId)
    }

}