package com.example.posts.data

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "comments")
data class Comment(
    @PrimaryKey @ColumnInfo(name = "id") val commentId: Int,
    val postId: Int,
    val name: String,
    val email: String,
    val body: String
)