package com.example.posts.data

class CommentRepository private constructor(private val commentDao: CommentDao){
    suspend fun getComment(commentId: Int) = commentDao.getComment(commentId)

    suspend fun saveComments(comments: List<Comment>) = commentDao.insertComment(comments)

    companion object {

        // For Singleton instantiation
        @Volatile private var instance: CommentRepository? = null

        fun getInstance(commentDao: CommentDao) =
            instance ?: synchronized(this) {
                instance ?: CommentRepository(commentDao).also { instance = it }
            }
    }
}