package com.example.posts.data

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(
    tableName = "Posts"
)
data class Posts(
    @PrimaryKey @ColumnInfo(name = "id") val postId: Int,
    val userId: Int,
    val title: String,
    val body: String
)