package com.example.posts.data

import com.google.gson.annotations.SerializedName

data class JsonPlaceholderPost(
    @field:SerializedName("userId") val userId: Int,
    @field:SerializedName("id") val id: Int,
    @field:SerializedName("title") val title: String,
    @field:SerializedName("body") val body: String
)