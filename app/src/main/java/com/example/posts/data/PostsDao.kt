package com.example.posts.data

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

@Dao
interface PostsDao {
    @Query("SELECT * FROM posts ORDER BY id")
    fun getPosts(): LiveData<List<Posts>>

    @Query("SELECT * FROM posts WHERE id = :postId")
    fun getPost(postId: Int): Posts

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(posts: Posts)

    @Query("SELECT * FROM posts")
    suspend fun getAllPostsAsync(): List<Posts>
}