package com.example.posts.data

import com.google.gson.annotations.SerializedName

data class JsonPlaceholderUsers(
    @field:SerializedName("id") val id: Int,
    @field:SerializedName("name") val name: String,
    @field:SerializedName("username") val username: String,
    @field:SerializedName("email") val email: String
)