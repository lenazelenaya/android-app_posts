package com.example.posts.data

import com.google.gson.annotations.SerializedName

data class JsonPlaceholderComment(
    @field:SerializedName("postId") val postId: Int,
    @field:SerializedName("id") val id: Int,
    @field:SerializedName("name") val name: String,
    @field:SerializedName("body") val body: String
)