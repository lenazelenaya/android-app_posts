package com.example.posts.api

import com.example.posts.data.JsonPlaceholderComment
import com.example.posts.data.JsonPlaceholderPost
import com.example.posts.data.JsonPlaceholderUsers
import com.example.posts.data.User
import com.example.posts.utilites.BASE_URL
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path

interface JsonPlaceholderService {
    @GET("/posts/{id}")
    fun getPostById(@Path("id") id: Int): JsonPlaceholderPost

    @GET("/posts")
    fun getPosts(): List<JsonPlaceholderPost>

    @GET("/users/{id}")
    fun getUserById(@Path("id") id: Int): JsonPlaceholderUsers

    @GET("/users")
    fun getUsers(): Call<List<User>>

    @GET("/posts/{id}/comment")
    fun getCommentsByPostId(@Path("id") id: Int): List<JsonPlaceholderComment>

    companion object {
        fun create(): JsonPlaceholderService {
            val logger =
                HttpLoggingInterceptor().apply { level = HttpLoggingInterceptor.Level.BASIC }

            val client = OkHttpClient.Builder()
                .addInterceptor(logger)
                .build()

            return Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(JsonPlaceholderService::class.java)
        }

    }
}